# Spring Boot for Open AI API

# Introduction
This project is a Spring Boot application that integrates with the OpenAI API.
The application allows you to call OpenAI API and chat with the Bot.

# Prerequisites
Before you begin, make sure you have the following tools installed on your system:

Java 17
Maven
An API key from OpenAI (https://beta.openai.com/)
# Installation
Clone the repository from GitLab to your local machine, using this link: https://gitlab.com/Azakaria1/test_chatgpt

# Application Running

Navigate to the project directory: **cd test_chatgpt**

If you work on Intellij or Eclipse, you can run the application using graphical interface.
Or you can use Mavan by running the following command to build and package the application: **mvn clean package**

Start the application using the following command: java -jar target/Test_IZICAP-0.0.1-SNAPSHOT.jar

Since we built a docker image for the application, usin terminal : `docker build -t izicap/chatgpt:latest .`,
you can now run it with the following command : `docker run -d -p 8080:8080 izicap/chatgpt:latest`

# Usage

Once the application is running, you can start making API requests to the OpenAI API.
First, you have to add your api key in `API_KEY` constant inside `APIUtils` class under the package `config` in the project
since Swagger's interface was not displayed, i used Postman ( plateforme pour l'utilisation des APIs ), 
you can now enter the following request URL : http://localhost:8080/ and specify the body in a JSON format :
`{
"input":"Enter your question here !"
}`

The API documentation for the OpenAI API can be found at https://beta.openai.com/docs/.

# Conclusion

With this project, we easily called the OpenAI API by building a chatbot from our Spring Boot application.
For more information about methods and logic used in this project, I commented each instruction I made.
