FROM openjdk:latest
EXPOSE 8080
WORKDIR /app/jar
COPY target/Test_IZICAP-*.jar /app.jar
CMD ["java","-jar","/app.jar"]
