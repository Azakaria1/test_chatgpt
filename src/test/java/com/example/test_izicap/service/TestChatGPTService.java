package com.example.test_izicap.service;

import com.example.test_izicap.model.record.CompletionResponse;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.context.WebApplicationContext;

import java.util.List;

import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.springframework.test.web.client.match.MockRestRequestMatchers.content;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

// REST API integration test using MockMvc
// we send a POST request to the / endpoint with a JSON payload in the request body.
// The response is then asserted to have an HTTP status code of 200 (OK)
@SpringBootTest
@AutoConfigureMockMvc
@RunWith(SpringRunner.class)
public class TestChatGPTService {

    @Autowired
    private WebApplicationContext context;
    @Autowired
    private MockMvc mvc;
    @Autowired
    private ObjectMapper objectMapper;

    public TestChatGPTService() {
    }

    @Before
    public void setUp() {
        mvc = MockMvcBuilders
                .webAppContextSetup(context)
                .build();
    }

    @Test
    public void testPostEndpoint() throws Exception {
        mvc.perform(MockMvcRequestBuilders.post("/")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content("{\"input\":\"what is github ?\"}"))
                .andExpect(status().isOk());
    }

    @Test
    public void testGetMethod() {
        List<String> lines = CSVService.read();
        assertNotNull(lines.subList(1,lines.size()),"File exists !");
        // validation of the response
    }

}