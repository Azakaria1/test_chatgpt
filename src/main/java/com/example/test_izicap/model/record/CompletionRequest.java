package com.example.test_izicap.model.record;

// Completion Request : where we configure our request to the API
// model : used by AI to produce the answer
// prompt : message
// temperature : predict the probability of the next word in the prompt
public record CompletionRequest(String model, String prompt, 
		double temperature, int max_tokens) {
	
	public static CompletionRequest defaultConfig(String input) {
		return new CompletionRequest("text-davinci-003", input, 0.7, 100);
	}
	
}
