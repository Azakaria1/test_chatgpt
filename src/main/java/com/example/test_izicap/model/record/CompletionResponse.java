package com.example.test_izicap.model.record;

import java.util.List;
import java.util.Optional;

// Completion Request : where we configure our response from the API as shown in the homework example
// model : used by AI to produce the answer
// prompt : message
// temperature : predict the probability of the next word in the prompt,
public record CompletionResponse(Usage usage, List<Choice> choices) {
	
	public Optional<String> firstAnswer() {
		if (choices == null || choices.isEmpty())
			return Optional.empty();
		return Optional.of(choices.get(0).text);
	}
	
	record Usage(int total_tokens, int prompt_tokens, int completion_tokens) {}
	
	record Choice(String text) {}
}
