package com.example.test_izicap.model.dto;

import lombok.Getter;

// class used to serialize the body of the request to be converted as an object in order to access the input value
@Getter
public class InputDTO {

    // the question we ask the AI API
    private String input;
}
