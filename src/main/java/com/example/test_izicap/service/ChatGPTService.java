package com.example.test_izicap.service;

import com.example.test_izicap.config.APIUtils;
import com.example.test_izicap.model.record.CompletionRequest;
import com.example.test_izicap.model.record.CompletionResponse;
import com.fasterxml.jackson.databind.ObjectMapper;
import jakarta.servlet.http.HttpServletResponse;
import lombok.AllArgsConstructor;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.net.URI;
import java.net.http.HttpClient;
import java.net.http.HttpRequest;
import java.net.http.HttpResponse;

// Service to handle interaction with OpenAI API
@Service
@AllArgsConstructor
public class ChatGPTService {

    // Api key I created from my OpenAI accoun
    private final ObjectMapper jsonMapper;

    private final HttpClient client = HttpClient.newHttpClient();

    public String sendPostRequest(String message) throws IOException, InterruptedException {

        // we build our request to the API using HttpRequest Builder by specifiyng the api url
        // the json content type header
        // the authorization header that contains our api key
        // and the body of the request containing the question as input
        HttpRequest request = HttpRequest.newBuilder().uri(URI.create(APIUtils.API_URL))
                .header(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON_VALUE)
                .header(HttpHeaders.AUTHORIZATION, "Bearer " + APIUtils.API_KEY)
                .POST(HttpRequest.BodyPublishers.ofString(message))
                .build();

        // we send the request after that in order to get the response body
        return client.send(request, HttpResponse.BodyHandlers.ofString()).body();
    }

    public String getAnswer(String message) throws Exception {

        // we configure the completion request with the config specified in the homework
        CompletionRequest completion = CompletionRequest.defaultConfig(message);
        String body = jsonMapper.writeValueAsString(completion);
        // we send the request
        String responseBody = sendPostRequest(body);

        // we used readValue method for converting the response to CompletionResponse
        // Then each param in the record get its value from the responseBody variable
        CompletionResponse completionResponse = jsonMapper.readValue(responseBody, CompletionResponse.class);

        String answer = completionResponse.firstAnswer().orElseThrow();
        // we build our line to insert it to the csv file
        String towriteinCSV ="\n" + message + CSVService.DELIMITER + answer.trim() ;

        CSVService.append(towriteinCSV);
        return answer;
    }

    public void getConversation(HttpServletResponse response) {
        CSVService.read().forEach(line -> {
            try {
                response.getWriter().write(line + "\n" );
            } catch (IOException e) {
                throw new RuntimeException(e);
            }
        });
    }
}