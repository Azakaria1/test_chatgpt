package com.example.test_izicap.service;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;
import java.util.StringTokenizer;

import static java.nio.file.StandardOpenOption.APPEND;
import static java.nio.file.StandardOpenOption.CREATE;

// service to handle csv file needed operations
public class CSVService {

    public static final String DELIMITER = ";"; // used to delimit csv file columns

    // file location
    public static final String CSVFILE = "C:\\Users\\Zack\\Desktop\\test_chatgpt\\src\\main\\resources\\static\\list.csv";

    public static List<String> read() {
        // we access the file
        Path file = Paths.get(CSVFILE);
        List<String> lines;
        List<String> modifiedLines = new ArrayList<>();

        try {
            // store file lines in a list of string
            lines = Files.readAllLines(file);

            // print the result in the body response of the request in a better way
            for (int i = 1; i <= lines.subList(1, lines.size()).size(); i++) {
                // print the file lines without the header
                StringTokenizer st = new StringTokenizer(lines.get(i), ";");
                modifiedLines.add("Question : " + st.nextToken()  + "\nResponse : " + st.nextToken() + "\n");
            }

        } catch (IOException e) {
            e.printStackTrace();
        }
        return modifiedLines;
    }

    public static void append(String line) {
        // we access firstly the file
        Path FILE = Paths.get(CSVFILE);
        try {
            // we append the question and the answer in the file
            // we used CREATE to create the file if it doesn't exist
            Files.write(FILE, line.getBytes(), APPEND, CREATE);
        } catch (IOException e) {
            System.err.println(e.getMessage());
        }
    }
}
