package com.example.test_izicap.web;

import com.example.test_izicap.model.dto.InputDTO;
import com.example.test_izicap.service.ChatGPTService;
import jakarta.servlet.http.HttpServletResponse;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;


@RestController
@RequiredArgsConstructor
public class ChatGptController {

    private final ChatGPTService chatGPTService;

    @GetMapping("/conversation")
    public void readConversation(HttpServletResponse response) {
        chatGPTService.getConversation(response);
    }

    // we create an endpoint in the main page of the website, we pass the input in the body on a JSON format
    @PostMapping("/")
    public ResponseEntity<?> queryAI(@RequestBody InputDTO input) throws Exception {
        // if the input is not null we recuperate the answer from the AI
        if (input.getInput() != null)
            return ResponseEntity.ok(chatGPTService.getAnswer(input.getInput()));
        return null;
    }

}
